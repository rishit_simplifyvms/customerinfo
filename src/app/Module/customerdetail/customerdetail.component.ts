import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { SharedcustomerService } from 'src/app/sharedcustomer.service';

@Component({
  selector: 'app-customerdetail',
  templateUrl: './customerdetail.component.html',
  styleUrls: ['./customerdetail.component.css']
})
export class CustomerdetailComponent implements OnInit {
  private data = from(require('../../../assets/users.json'));
   responseData = [];
   index: any;
   customerClicked: boolean;
  constructor(private shared: SharedcustomerService) { }

  ngOnInit(): void {
    this.data.subscribe(response => {
      this.responseData.push(response);
    })
    this.index = this.shared.getIndex();
    this.customerClicked = this.shared.getCustomerclicked();
  }

  next(){
    if(this.index < this.responseData.length - 1)
      this.index++;
  }

  previous() {
    if(this.index > 0)
      this.index--;
  }

}
