import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { SharedcustomerService } from 'src/app/sharedcustomer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  private data = from(require('../../../assets/users.json'));
   responseData = [];
   ind: Number;
  constructor( private shared: SharedcustomerService, private router: Router) {}

  ngOnInit(): void {
    this.data.subscribe(response => {
      this.responseData.push(response);
    })
  }

  customerDetail(id: any) {
    this.responseData.map((user,index) => {
      if(user.id == id ){
        this.ind = index;
      }
    })
    this.shared.setIndex(this.ind);
    this.shared.setCustomerClicked(true);
    this.router.navigate(['customerIndividual'])
  }

}
