import { Component, OnInit } from '@angular/core';
import { SharedcustomerService } from 'src/app/sharedcustomer.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  act:boolean =true;

  constructor(private shared: SharedcustomerService) { }

  ngOnInit(): void {
  }

  set() {
    this.shared.setCustomerClicked(false);
    this.act = false;
  }

  acti() {
    this.act = true;
  }

}
