import { TestBed } from '@angular/core/testing';

import { SharedcustomerService } from './sharedcustomer.service';

describe('SharedcustomerService', () => {
  let service: SharedcustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SharedcustomerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
