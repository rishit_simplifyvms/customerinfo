import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerComponent } from './module/customer/customer.component';
import { CustomerdetailComponent } from './module/customerdetail/customerdetail.component';

const routes: Routes = [
  {path:'', component: CustomerComponent},
  {path: 'customerIndividual', component: CustomerdetailComponent},
  {path: 'customerDetail', component: CustomerdetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
