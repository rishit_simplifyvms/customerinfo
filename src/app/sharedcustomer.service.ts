import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedcustomerService {
  private index : Number; 
  private customerClicked: boolean = false;
  constructor() { }
  setIndex(ind: Number){
    this.index = ind;
  }
  setCustomerClicked(bool: boolean) {
    this.customerClicked = bool;
  }
  getCustomerclicked() {
    return this.customerClicked;
  }
  getIndex() {
    return this.index;
  }
}
